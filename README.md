# README #

This repository contains Email service, which is used to send email using [Sendgrid](https://sendgrid.com/).

### Confluence link:###  https://ebctech.atlassian.net/wiki/spaces/TAPS/pages/30277675/Email+Service

### JIRA Link: ### https://ebctech.atlassian.net/browse/TAPS-1044?filter=-1

### How do I get set up? ###

* How to run
   Command: sudo sbt run <Port No>
* Configuration
   -> application.conf 
         play.crypto.secret = < Your secret key>
         sendgrid.templateID.orderSuccess = <Sendgrid template id in case of order success mail>
         sendgrid.templateID.orderFailure = <Sendgrid template id in case of order failure mail>
         sendgrid.templateID.contactUs = <Sendgrid template id in case of contact us mail>
         sendgrid.templateID.helpdeskSupport = <Sendgrid template id in case of helpdesk support mail>

         sendgrid.API.Key = <Key associated with Sendgrid's account>
* Dependencies
   Json library: "net.liftweb" %% "lift-json" % "2.6.3"
   Sendgrid library: "com.sendgrid" % "sendgrid-java" % "3.0.8"
   Testkit libraries: "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test, "com.typesafe.akka" %% "akka-testkit" % "2.3.9" % "test",

   Kamon libraries: "io.kamon" %% "kamon-core" % "0.6.3",
  "io.kamon" %% "kamon-spm" % "0.6.3",
  "io.kamon" %% "kamon-akka" % "0.6.3",
  "io.kamon" %% "kamon-system-metrics" % "0.6.3",
  "org.apache.thrift" % "libthrift" % "0.9.2"

* How to run tests : Command: sudo sbt test

### Who do I talk to? ###

* Repo owner or admin : priya@ebctechnologies.com