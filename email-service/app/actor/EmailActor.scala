package actor

import java.io.IOException

import akka.actor._
import com.sendgrid._
import com.typesafe.config.ConfigFactory
import kamon.Kamon
import kamon.trace.Tracer
import net.liftweb.json.{NoTypeHints, Serialization}
import org.slf4j.{Logger, LoggerFactory}


object EmailActor {
  var logger: Logger = LoggerFactory.getLogger(this.getClass);

  case class OrderSuccessMsg(mailto: String,
                             mailfrom: String,
                             mailcc: String,
                             mailbcc: String,
                             subject: String,
                             name: String,
                             orderID: String,
                             productString: Seq[String],
                             subTotal: String,
                             saleTax: String,
                             total: String,
                             phoneNumber: String,
                             address: String,
                             city: String,
                             state: String,
                             zip: String,
                             country: String)

  case class OrderFailureMsg(mailto: String,
                             mailfrom: String,
                             mailcc: String,
                             mailbcc: String,
                             subject: String,
                             name: String,
                             orderErrorMessage: String,
                             orderID: String,
                             productString: Seq[String],
                             subTotal: String,
                             saleTax: String,
                             total: String,
                             phoneNumber: String,
                             address: String,
                             city: String,
                             state: String,
                             zip: String,
                             country: String)

  case class ContactUsMsg(mailto: String,
                          mailfrom: String,
                          mailcc: String,
                          mailbcc: String,
                          subject: String,
                          name: String)

  case class HelpdeskSupportMsg(mailto: String,
                                mailfrom: String,
                                mailcc: String,
                                mailbcc: String,
                                subject: String,
                                name: String,
                                content: String,
                                userEmail: String)

}


class EmailActor extends Actor with ActorLogging {

  import actor.EmailActor._

  implicit var formats = Serialization.formats(NoTypeHints)
  val config = ConfigFactory.load()
  val sendgridApiKey = config.getString("sendgrid.API.Key")
  val orderSuccessTemplate = config.getString("sendgrid.templateID.orderSuccess")
  val orderFailureTemplate = config.getString("sendgrid.templateID.orderFailure")
  val contactUsTemplate = config.getString("sendgrid.templateID.contactUs")
  val helpdeskSupportTemplate = config.getString("sendgrid.templateID.helpdeskSupport")

  def receive = {
    case OrderSuccessMsg(mailto, mailfrom, mailcc, mailbcc, subject, name, orderID, productString, subTotal, saleTax, total, phoneNumber, address, city, state, zip, country) =>
      val tContext = Kamon.tracer.newContext("orderSuccessExecution")
      val segment = tContext.startSegment("orderSuccessExecution", "orderSuccessExecution", "kamon")
      Tracer.withNewContext("orderSuccessExecution") {
        sender() ! orderSuccess(mailto, mailfrom, mailcc, mailbcc, subject, name, orderID, productString, subTotal, saleTax, total, phoneNumber, address, city, state, zip, country)
      }
      segment.finish()
      tContext.finish()

    case OrderFailureMsg(mailto, mailfrom, mailcc, mailbcc, subject, name, orderErrorMessage, orderID, productString, subTotal, saleTax, total, phoneNumber, address, city, state, zip, country) =>
      val tContext = Kamon.tracer.newContext("OrderFailurexecution")
      val segment = tContext.startSegment("orderSuccessExecution", "OrderFailureExecution", "kamon")
      Tracer.withNewContext("OrderFailureExecution") {
        sender() ! orderFailure(mailto, mailfrom, mailcc, mailbcc, subject, name, orderErrorMessage, orderID, productString, subTotal, saleTax, total, phoneNumber, address, city, state, zip, country)
      }
      segment.finish()
      tContext.finish()

    case ContactUsMsg(mailto, mailfrom, mailcc, mailbcc, subject, name) =>
      val tContext = Kamon.tracer.newContext("ContactUsExecution")
      val segment = tContext.startSegment("ContactUsExecution", "orderSuccessExecution", "kamon")
      Tracer.withNewContext("ContactUsExecution") {
        sender() ! contactUs(mailto, mailfrom, mailcc, mailbcc, subject, name)
      }
      segment.finish()
      tContext.finish()

    case HelpdeskSupportMsg(mailto, mailfrom, mailcc, mailbcc, subject, name, content, userEmail) =>
      val tContext = Kamon.tracer.newContext("HelpdeskSupportExecution")
      val segment = tContext.startSegment("HelpdeskSupportExecution", "orderSuccessExecution", "kamon")
      Tracer.withNewContext("HelpdeskSupportExecution") {
        sender() ! helpdeskSupport(mailto, mailfrom, mailcc, mailbcc, subject, name, content, userEmail)
      }
      segment.finish()
      tContext.finish()
  }

  def orderSuccess(mailto: String,
                   mailfrom: String,
                   mailcc: String,
                   mailbcc: String,
                   subject: String,
                   name: String,
                   orderID: String,
                   productString: Seq[String],
                   subTotal: String,
                   saleTax: String,
                   total: String,
                   phoneNumber: String,
                   address: String,
                   city: String,
                   state: String,
                   zip: String,
                   country: String): String = {

    val mail = new Mail()
    val from = new Email(mailfrom)
    val to = new Email(mailto)
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.setTemplateId(orderSuccessTemplate)

    logger.info("Order Success Template Id : " + orderSuccessTemplate);
    val personalization = new Personalization
    personalization.addTo(to)
    personalization.addSubstitution(":name", name)
    personalization.addSubstitution(":orderID", orderID)

    personalization.addSubstitution(":productString0", productString(0))
    personalization.addSubstitution(":productString1", productString(1))
    personalization.addSubstitution(":productString2", productString(2))
    personalization.addSubstitution(":productString3", productString(3))
    personalization.addSubstitution(":productString4", productString(4))
    personalization.addSubstitution(":productString5", productString(5))
    personalization.addSubstitution(":productString6", productString(6))
    personalization.addSubstitution(":productString7", productString(7))
    personalization.addSubstitution(":productString8", productString(8))
    personalization.addSubstitution(":productString9", productString(9))

    personalization.addSubstitution(":subTotal", subTotal)
    personalization.addSubstitution(":saleTax", saleTax)
    personalization.addSubstitution(":total", total)
    personalization.addSubstitution(":phoneNumber", phoneNumber)
    personalization.addSubstitution(":address", address)
    personalization.addSubstitution(":city", city)
    personalization.addSubstitution(":state", state)
    personalization.addSubstitution(":zip", zip)
    personalization.addSubstitution(":country", country)

    if (!mailcc.isEmpty) {
      val ccArray = mailcc.split(",")
      for (value <- ccArray) {
        val cc = new Email(value)
        personalization.addCc(cc)
      }
    }
    if (!mailbcc.isEmpty) {
      val bccArray = mailbcc.split(",")
      for (value <- bccArray) {
        val bcc = new Email(value)
        personalization.addBcc(bcc)
      }
    }
    mail.addPersonalization(personalization)

    val sendGrid = new SendGrid(sendgridApiKey)
    val request = new Request()

    request.method = Method.POST
    request.endpoint = "mail/send"
    request.body = mail.build()
    try {
      val response: Response = sendGrid.api(request)
      val res = response.statusCode.toString()
      return res;
    } catch {
      case ex: IOException => {
        logger.error("Exception : " + ex)
        return "400": String
      }
      case ex: Exception => {
        logger.error("Exception : " + ex)
        return "500": String
      }
    }
  }

  def orderFailure(mailto: String,
                   mailfrom: String,
                   mailcc: String,
                   mailbcc: String,
                   subject: String,
                   name: String,
                   orderErrorMessage: String,
                   orderID: String,
                   productString: Seq[String],
                   subTotal: String,
                   saleTax: String,
                   total: String,
                   phoneNumber: String,
                   address: String,
                   city: String,
                   state: String,
                   zip: String,
                   country: String): String = {

    val mail = new Mail()
    val from = new Email(mailfrom)
    val to = new Email(mailto)
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.setTemplateId(orderFailureTemplate)

    logger.info("Order Failure Template Id : " + orderFailureTemplate);

    val personalization = new Personalization
    personalization.addTo(to)
    personalization.addSubstitution(":name", name)
    personalization.addSubstitution(":orderErrorMessage", orderErrorMessage)
    personalization.addSubstitution(":orderID", orderID)

    personalization.addSubstitution(":productString0", productString(0))
    personalization.addSubstitution(":productString1", productString(1))
    personalization.addSubstitution(":productString2", productString(2))
    personalization.addSubstitution(":productString3", productString(3))
    personalization.addSubstitution(":productString4", productString(4))
    personalization.addSubstitution(":productString5", productString(5))
    personalization.addSubstitution(":productString6", productString(6))
    personalization.addSubstitution(":productString7", productString(7))
    personalization.addSubstitution(":productString8", productString(8))
    personalization.addSubstitution(":productString9", productString(9))

    personalization.addSubstitution(":subTotal", subTotal)
    personalization.addSubstitution(":saleTax", saleTax)
    personalization.addSubstitution(":total", total)
    personalization.addSubstitution(":phoneNumber", phoneNumber)
    personalization.addSubstitution(":address", address)
    personalization.addSubstitution(":city", city)
    personalization.addSubstitution(":state", state)
    personalization.addSubstitution(":zip", zip)
    personalization.addSubstitution(":country", country)

    if (!mailcc.isEmpty) {
      val ccArray = mailcc.split(",")
      for (value <- ccArray) {
        val cc = new Email(value)
        personalization.addCc(cc)
      }
    }
    if (!mailbcc.isEmpty) {
      val bccArray = mailbcc.split(",")
      for (value <- bccArray) {
        val bcc = new Email(value)
        personalization.addBcc(bcc)
      }
    }

    mail.addPersonalization(personalization)

    val sendGrid = new SendGrid(sendgridApiKey)
    val request = new Request()

    request.method = Method.POST
    request.endpoint = "mail/send"
    request.body = mail.build()

    try {
      val response: Response = sendGrid.api(request)
      val res = response.statusCode.toString()
      return res;
    } catch {
      case ex: IOException => {
        logger.error("Exception : " + ex)
        return "400": String
      }
      case ex: Exception => {
        logger.error("Exception : " + ex)
        return "500": String
      }
    }
  }

  def contactUs(mailto: String,
                mailfrom: String,
                mailcc: String,
                mailbcc: String,
                subject: String,
                name: String): String = {

    val mail = new Mail()
    val from = new Email(mailfrom)
    val to = new Email(mailto)
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.setTemplateId(contactUsTemplate)
    logger.info("ContactUs Template Id : " + contactUsTemplate);

    val personalization = new Personalization
    personalization.addTo(to)
    personalization.addSubstitution(":name", name)

    if (!mailcc.isEmpty) {
      val ccArray = mailcc.split(",")
      for (value <- ccArray) {
        val cc = new Email(value)
        personalization.addCc(cc)
      }
    }
    if (!mailbcc.isEmpty) {
      val bccArray = mailbcc.split(",")
      for (value <- bccArray) {
        val bcc = new Email(value)
        personalization.addBcc(bcc)
      }
    }

    mail.addPersonalization(personalization)

    val sendGrid = new SendGrid(sendgridApiKey)
    val request = new Request()

    request.method = Method.POST
    request.endpoint = "mail/send"
    request.body = mail.build()
    try {
      val response: Response = sendGrid.api(request)
      val res = response.statusCode.toString()
      return res;
    } catch {
      case ex: IOException => {
        logger.error("Exception : " + ex)
        return "400": String
      }
      case ex: Exception => {
        logger.error("Exception : " + ex)
        return "500": String
      }
    }
  }

  def helpdeskSupport(mailto: String,
                      mailfrom: String,
                      mailcc: String,
                      mailbcc: String,
                      subject: String,
                      name: String,
                      content: String,
                      userEmail: String): String = {

    val mail = new Mail()
    val from = new Email(mailfrom)
    val to = new Email(mailto)
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.setTemplateId(helpdeskSupportTemplate)
    logger.info("HelpDesk Support Template Id : " + helpdeskSupportTemplate);

    val personalization = new Personalization
    personalization.addTo(to)
    personalization.addSubstitution(":name", name)
    personalization.addSubstitution(":content", content)
    personalization.addSubstitution(":userEmail", userEmail)

    if (!mailcc.isEmpty) {
      val ccArray = mailcc.split(",")
      for (value <- ccArray) {
        val cc = new Email(value)
        personalization.addCc(cc)
      }
    }
    if (!mailbcc.isEmpty) {
      val bccArray = mailbcc.split(",")
      for (value <- bccArray) {
        val bcc = new Email(value)
        personalization.addBcc(bcc)
      }
    }

    mail.addPersonalization(personalization)

    val sendGrid = new SendGrid(sendgridApiKey)
    val request = new Request()

    request.method = Method.POST
    request.endpoint = "mail/send"
    request.body = mail.build()
    try {
      val response: Response = sendGrid.api(request)
      val res = response.statusCode.toString()
      return res;
    } catch {
      case ex: IOException => {
        logger.error("Exception : " + ex)
        return "400": String
      }
      case ex: Exception => {
        logger.error("Exception : " + ex)
        return "500": String
      }
    }
  }

}
