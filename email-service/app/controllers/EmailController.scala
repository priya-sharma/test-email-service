package controllers

import javax.inject._

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import actor.EmailActor
import actor.EmailActor.{ContactUsMsg, HelpdeskSupportMsg, OrderFailureMsg, OrderSuccessMsg}
import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import entity.{ContactUsMessage, HelpdeskSupportMessage, OrderFailureMessage, OrderSuccessMessage}
import kamon.Kamon
import kamon.trace.Tracer
import net.liftweb.json.Serialization.write
import net.liftweb.json._
import play.api.mvc._

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

    
/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class EmailController @Inject()(as : ActorSystem, ex : ExecutionContext) extends Controller {
  implicit val timeout: Timeout = 50.seconds
  implicit var formats = Serialization.formats(NoTypeHints) 
  val system = as
  val emailActor = system.actorOf(Props[EmailActor])
  var logger :Logger = LoggerFactory.getLogger(this.getClass);




  def orderSuccess() =Action {
    request =>
      //Traces and custom metrics
      val tContext = Kamon.tracer.newContext("orderSuccess")
      val segment = tContext.startSegment("orderSuccess", "business-logic", "kamon")
      logger.debug("Order success email request received.............")
      logger.debug("Request data is : "+request);

      Tracer.withNewContext("orderSuccess") {
        val orderSuccessMessage = getOrderSuccessMessage(request)
        val future = emailActor ? OrderSuccessMsg(orderSuccessMessage.mailto,
          orderSuccessMessage.mailfrom,
          orderSuccessMessage.mailcc,
          orderSuccessMessage.mailbcc,
          orderSuccessMessage.subject,
          orderSuccessMessage.name,
          orderSuccessMessage.orderID,
          orderSuccessMessage.productString,
          orderSuccessMessage.subTotal,
          orderSuccessMessage.saleTax,
          orderSuccessMessage.total,
          orderSuccessMessage.phoneNumber,
          orderSuccessMessage.address,
          orderSuccessMessage.city,
          orderSuccessMessage.state,
          orderSuccessMessage.zip,
          orderSuccessMessage.country)
        val response = Await.result(future, timeout.duration).toString()
        logger.debug("Order success email request completed.............")
        logger.debug("Generated : " + response)

        segment.finish()
        tContext.finish()
        Ok(write(response))

      }

  }

  def orderFailure() =Action {
    request =>
      logger.debug("Order failure email request received.............")
      logger.debug("Request data is : "+request);
      val orderFailureMessage = getOrderFailureMessage(request)
      val future  = emailActor ? OrderFailureMsg(orderFailureMessage.mailto,
        orderFailureMessage.mailfrom,
        orderFailureMessage.mailcc,
        orderFailureMessage.mailbcc,
        orderFailureMessage.subject,
        orderFailureMessage.name,
        orderFailureMessage.orderErrorMessage,
        orderFailureMessage.orderID,
        orderFailureMessage.productString,
        orderFailureMessage.subTotal,
        orderFailureMessage.saleTax,
        orderFailureMessage.total,
        orderFailureMessage.phoneNumber,
        orderFailureMessage.address,
        orderFailureMessage.city,
        orderFailureMessage.state,
        orderFailureMessage.zip,
        orderFailureMessage.country)
      val response = Await.result(future, timeout.duration).toString()
      logger.debug("Order failure email request completed.............")
      logger.debug("Generated : "+response)
      Ok(write(response))
  }
  
  def contactUs() =Action {
    request =>
      logger.debug("ContactUs email request received.............")
      logger.debug("Request data is : "+request);
    val contactUsMessage = getContactUsMessage(request)
    val future  = emailActor ? ContactUsMsg(contactUsMessage.mailto,
      contactUsMessage.mailfrom,
      contactUsMessage.mailcc,
      contactUsMessage.mailbcc,
      contactUsMessage.subject,
      contactUsMessage.name)
    val response = Await.result(future, timeout.duration).toString()
      logger.debug("ContactUs email request completed.............")
      logger.debug("Generated : "+response)
    Ok(write(response))
  }

  def helpdeskSupport() =Action {
    request =>
      logger.debug("Helpdesk support email request received.............")
      logger.debug("Request data is : "+request);
      val helpdeskSupportMessage = getHelpdeskSupportMessage(request)
      val future  = emailActor ? HelpdeskSupportMsg(helpdeskSupportMessage.mailto,
        helpdeskSupportMessage.mailfrom,
        helpdeskSupportMessage.mailcc,
        helpdeskSupportMessage.mailbcc,
        helpdeskSupportMessage.subject,
        helpdeskSupportMessage.name,
        helpdeskSupportMessage.content,
        helpdeskSupportMessage.userEmail)
      val response = Await.result(future, timeout.duration).toString()
      logger.debug("Helpdesk support email request completed.............")
      logger.debug("Generated : "+response)
      Ok(write(response))
  }
  
  def getOrderSuccessMessage(request: Request[AnyContent]): OrderSuccessMessage = {
    val json = request.body.asText.get;
    val jsonOb = JsonParser.parse(json);
    val orderSuccessMessage = jsonOb.extract[OrderSuccessMessage]
    return orderSuccessMessage;
  }

  def getOrderFailureMessage(request: Request[AnyContent]): OrderFailureMessage = {
    val json = request.body.asText.get;
    val jsonOb = JsonParser.parse(json);
    val orderFailureMessage = jsonOb.extract[OrderFailureMessage]
    return orderFailureMessage;
  }

  def getContactUsMessage(request: Request[AnyContent]): ContactUsMessage = {
    val json = request.body.asText.get;
    val jsonOb = JsonParser.parse(json);
    val contactUsMessage = jsonOb.extract[ContactUsMessage]
    return contactUsMessage;
  }

  def getHelpdeskSupportMessage(request: Request[AnyContent]): HelpdeskSupportMessage = {
    val json = request.body.asText.get;
    val jsonOb = JsonParser.parse(json);
    val helpdeskSupportMessage = jsonOb.extract[HelpdeskSupportMessage]
    return helpdeskSupportMessage;
  }
}
