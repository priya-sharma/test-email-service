package entity

case class ContactUsMessage(mailto : String,
                            mailfrom : String,
                            mailcc : String,
                            mailbcc : String,
                            subject : String,
                            name : String)