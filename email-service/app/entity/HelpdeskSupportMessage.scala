package entity

case class HelpdeskSupportMessage(mailto : String,
                                  mailfrom : String,
                                  mailcc : String,
                                  mailbcc : String,
                                  subject : String,
                                  name : String,
                                  content : String,
                                  userEmail : String)