package entity

case class OrderFailureMessage(mailto : String,
                               mailfrom : String,
                               mailcc : String,
                               mailbcc : String,
                               subject : String,
                               name : String,
                               orderErrorMessage : String,
                               orderID : String,
                               productString : Seq[String],
                               subTotal : String,
                               saleTax : String,
                               total : String,
                               phoneNumber : String,
                               address : String,
                               city : String,
                               state : String,
                               zip : String,
                               country : String)