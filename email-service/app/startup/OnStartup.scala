package startup

import com.google.inject.{Inject, Singleton}
import kamon.Kamon
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle

/**
  * Created by spineor on 23/5/17.
  */
@Singleton
class OnStartup @Inject()(applicationLifeCycle: ApplicationLifecycle){
  var logger :Logger = LoggerFactory.getLogger(this.getClass);

  logger.info("*************** Starting Kamon Sevice ******************")
  Kamon.start()

}


