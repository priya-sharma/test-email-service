name := """email-service"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

enablePlugins(sbtdocker.DockerPlugin)

dockerAutoPackageJavaApplication()

scalaVersion := "2.11.7"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"
libraryDependencies += "ch.qos.logback" % "logback-parent" % "1.2.2"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "net.liftweb" %% "lift-json" % "2.6.3",
  "com.sendgrid" % "sendgrid-java" % "3.0.8",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "com.typesafe.akka" %% "akka-testkit" % "2.3.9" % "test",


  // #KAMON-SEMATEXT####################################
  "io.kamon" %% "kamon-core" % "0.6.3",
  "io.kamon" %% "kamon-spm" % "0.6.3",
  "io.kamon" %% "kamon-akka" % "0.6.3",
  "io.kamon" %% "kamon-system-metrics" % "0.6.3",
  "org.apache.thrift" % "libthrift" % "0.9.2"

)



fork in run := false
