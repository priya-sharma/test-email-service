import actor.EmailActor
import actor.EmailActor.{ContactUsMsg, HelpdeskSupportMsg, OrderFailureMsg, OrderSuccessMsg}
import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import net.liftweb.json.{NoTypeHints, Serialization}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Await
import scala.concurrent.duration._

class EmailTestCases(_system: ActorSystem) extends TestKit(_system)
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  var logger :Logger = LoggerFactory.getLogger("Test");
  implicit var formats = Serialization.formats(NoTypeHints)
  implicit val timeout = Timeout(50 seconds)

  def this() = this(ActorSystem("MySpec"))


  override def afterAll {
    system.terminate()
  }

/*
To Test Success Cases. Using Correct Email Ids. All Must Return 200 or 202
 */
  "Email Service (Success Test)" must {
    logger.info("Email test cases (Email Service (Success Test))started........................")
    val fromMail = "cs@theautopartsshop.com"
    val toMail = "amitpruthi@ebctechnologies.com"
    val toMail2 = "amit.pruthi@spineor.com"
    val toMail3 = ""
    val userMailId = "priya@ebctechnologies.com"

    var successMailContent = new OrderSuccessMsg(toMail, fromMail, toMail2, toMail3, "Order Detail", "Success Test", "123-7854",
      Seq("<b>Your Order Was Successfully Added</b>",null, null,null, null,null, null,null, null, null), "500", "21", "521", "91 1234567889", "Xyz Street", "Test City", "TestingState"
      , "123524", "UnknownConuntry")

    var orderFailContent = new OrderFailureMsg(toMail, fromMail, toMail2, toMail3, "Order Fail", "FaiLOrderPro", "<b> Sorry, your Order Was Not Successfull.</b>","123-7854",
      Seq("<b>Your Order Was Successfully Added</b>",null, null,null, null,null, null,null, null, null), "500", "21", "521", "91 1234567889", "Xyz Street", "Test City", "TestingState"
      , "123524", "UnknownConuntry")

    var contactUsContent = new ContactUsMsg(toMail, fromMail, toMail2, toMail3, "ContactMe Request", "ContactMe-Urgent")

    var helpDeskContent = new HelpdeskSupportMsg(toMail, fromMail, toMail2, toMail3, "xyz Client", "HelpDesk Live Support",
      "<b>Please resolve my query ASAP</b>", userMailId)

    val emailActor = system.actorOf(Props[EmailActor])
    /*
    To Verify If Order Success Mail Working Or Not
     */
    "Send Order Success Mail (Success Test)" in {
      val future  = emailActor ? successMailContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response should be 200 or 202 but ") {
        response must ((be("200")) or (be("202")))
      }
    }

    /*
    To Verify If Order Fail Mail Working Or Not
     */
    "Send Order Fail Mail (Success Test)" in {
      val future  = emailActor ? orderFailContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response should be 200 or 202 but ") {
        response must ((be("200")) or (be("202")))
      }
    }

    /*
   To Verify If ContactUs Mail Working Or Not
    */
    "Send ContactUs Mail (Success Test)" in {
      val future  = emailActor ? contactUsContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response should be 200 or 202 but ") {
        response must ((be("200")) or (be("202")))
      }
    }

    /*
   To Verify If Help Desk Support Mail Working Or Not
    */
    "Send HelpDesk Mail (Success Test)" in {
      val future  = emailActor ? helpDeskContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response should be 200 or 202 but ") {
        response must ((be("200")) or (be("202")))
      }
    }
    logger.info("Email test cases (Email Service (Success Test)) completed........................")
  }

  /*
  Test With Wrong Email Id. To Test Wrong Cases. Test With Using Invalid Email Id (toMail). . All Must Not Return 200 or 202 (Throws Exception
   */
  "Email Service (Fail Test)" must {
    logger.info("Email test cases (Email Service (Fail Test)) started........................")
    val fromMail = "cs@theautopartsshop.com"
    val toMail = "amitpruthiebctechnologies.com" // Wrong Email ID
    val toMail2 = "amit.pruthi@spineor.com"
    val toMail3 = ""
    val userMailId = "priya@ebctechnologies.com"

    var successMailContent = new OrderSuccessMsg(toMail, fromMail, toMail2, toMail3, "Order Detail", "Success Test", "123-7854",
      Seq("<b>Your Order Was Successfully Added</b>",null, null,null, null,null, null,null, null, null), "500", "21", "521", "91 1234567889", "Xyz Street", "Test City", "TestingState"
      , "123524", "UnknownConuntry")

    var orderFailContent = new OrderFailureMsg(toMail, fromMail, toMail2, toMail3, "Order Fail", "FaiLOrderPro", "<b> Sorry, your Order Was Not Successfull.</b>","123-7854",
      Seq("<b>Your Order Was Successfully Added</b>",null, null,null, null,null, null,null, null, null), "500", "21", "521", "91 1234567889", "Xyz Street", "Test City", "TestingState"
      , "123524", "UnknownConuntry")

    var contactUsContent = new ContactUsMsg(toMail, fromMail, toMail2, toMail3, "ContactMe Request", "ContactMe-Urgent")

    var helpDeskContent = new HelpdeskSupportMsg(toMail, fromMail, toMail2, toMail3, "xyz Client", "HelpDesk Live Support",
      "<b>Please resolve my query ASAP</b>", userMailId)

    var successMailContentWithToAndCCSame = new OrderSuccessMsg(toMail, fromMail, toMail, toMail, "Order Detail", "Success Test", "123-7854",
      Seq("<b>Your Order Was Successfully Added</b>",null, null,null, null,null, null,null, null, null), "500", "21", "521", "91 1234567889", "Xyz Street", "Test City", "TestingState"
      , "123524", "UnknownConuntry")

    val emailActor = system.actorOf(Props[EmailActor])

    /*
   To Verify If Order Success Mail Working Or Not
    */
    "Send Order Success Mail With Same CC and To Email id (But Both Should Not Be Same) (Fail Test)" in {
      val future  = emailActor ? successMailContentWithToAndCCSame
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response must not be 200 or 202 but ") {
        response must not ((be("200")) or (be("202")))
      }
    }

    /*
    To Verify If Order Success Mail Working Or Not
     */
    "Send Order Success Mail (Fail Test)" in {
      val future  = emailActor ? successMailContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response must not be 200 or 202 but ") {
        response must not ((be("200")) or (be("202")))
      }
    }

    /*
    To Verify If Order Fail Mail Working Or Not
     */
    "Send Order Fail Mail (Fail Test)" in {
      val future  = emailActor ? orderFailContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response must not be 200 or 202 but ") {
        response must not ((be("200")) or (be("202")))
      }
    }

    /*
   To Verify If ContactUs Mail Working Or Not
    */
    "Send ContactUs Mail (Fail Test)" in {
      val future  = emailActor ? contactUsContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response must not be 200 or 202 but ") {
        response must not ((be("200")) or (be("202")))
      }
    }

    /*
   To Verify If Help Desk Support Mail Working Or Not
    */
    "Send HelpDesk Mail (Fail Test)" in {
      val future  = emailActor ? helpDeskContent
      val response = Await.result(future, timeout.duration).toString()
      logger.info("Response : "+response)
      response must not ((be (null)) or (be (empty)))
      withClue("Response must not be 200 or 202 but ") {
        response must not ((be("200")) or (be("202")))
      }
    }
    logger.info("Email test cases (Email Service (Fail Test)) completed........................")
  }

}

